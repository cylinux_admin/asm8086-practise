.model .small
.8086
;Define the data segment.
data segment
    string db 19h, 11h, 80h, 91h, 38h, 88h, 0ffh, '$'
    positive_number db 7 dup(0)
    negative_number db 7 dup(0)
data ends

;Define the stack segment.               
stack segment
    top dw 64h dup(0)
stack ends                     

;Define the code segment.
code segment
assume cs:code, ds:data, ss:stack
main proc far                    
    mov ax, data
    mov ds, ax
    mov bx, 0000h
    mov cl, 00h
classify:      
    mov al, cl
    mov bx, offset string
    xlat 
    cmp al, '$'
    jz  exit
    cmp al, 0
    jg  greater
    inc bl
    mov negative_number, bl
    mov si, bx
    and si, 00ffh
    mov negative_number [si], al 
    inc cl
    jmp classify
greater:    
    inc bh
    mov positive_number, bh   
    mov si, bx
    and si, 0ff00h
    mov positive_number [0], al          
    inc cl
    jmp classify
exit:
    hlt    
endp  
code ends    
end




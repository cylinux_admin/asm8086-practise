.model .small
.8086    
;define the data segment
data segment
var1 dw 1011001100001010b        
var2 dw 0
data ends               

;define the code segment
code segment            
    assume cs:code, ds:data
main proc far
    mov ax, data
    mov ds, ax
    mov cx, 20
    mov ax, var1
    mov dx, var1
    xor dx, 0ffffh
    mov var1, dx
    mov bx, 0000h
lop:            
    shl ax, 1
    jnc handle
    inc bx    
handle:
    loop lop        
endp        
code ends    
    end
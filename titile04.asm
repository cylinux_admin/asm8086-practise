.model .small
.8086    
;define the data segment
data segment
dat1 db 11h, 22h, 33h, 44h, 55h
     db 66h, 77h, 78h, 79h, 10h
     db 21h, 01h, 78h, 31h, 00h
max  db 0
min  db 0         
data ends               

;define the code segment
code segment            
    assume cs:code, ds:data
main proc far
    mov ax, data
    mov ds, ax     
    mov cx, 15
    lea si, dat1   
    mov bl, 7fh
    mov bh, 00h
lop:
    mov al, [si]        
    mov dl, al
    sar dl, 1
    jnc even_number
    cmp al, bl
    jg  next
    mov bl, al       
    jmp next
even_number: 
    cmp al, bh
    jng next
    mov bh, al
next:         
    inc si
    loop lop
    mov max, bh
    mov min, bl
endp        
code ends  
    end
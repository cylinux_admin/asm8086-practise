;; 在一计算机测控系统中，用8253计数器0作定时器，用计数器1对外部事件进行计数。
;; 计数器0通过8259A中断控制器的IR3向8088CPU申请中断，在中断服务程序中，每隔1秒读取一次计数器1的二进制计数值，将其累加到BUF开始的四个字节单元，同时清零计数器1。
;;    设CLK0上时钟为1MHz。
;;    计数器0定时间隔为20ms。
;;    8253口地址  0340H～0343H。
;;    8259A口地址 0320H～0321H。
;;    中断类型码高5位 00101B。
;;① 编写完成上述功能的8253、8259A等初始化程序。
;;② 编程设置中断入口地址。
;;③ 编写中断服务程序完成相应功能。
;; 1，写初始化程序
        ;; initilize 8253
        mov dx, 0343h
        mov al, 00110110b
        out dx, al

        mov al, 01110000b
        out dx, al

        mov dx, 0341h
        mov al, 00h
        out dx, al
        out dx, al

        mov dx, 0340h
        mov ax, 50000
        out dx, al
        mov al, ah
        out dx, al

        ;; initilize 8259
        mov al, 00010011b
        mov dx, 0320h
        out dx, al

        mov al, 00101000b
        mov dx, 0321h
        out dx, al

        mov al, 000000001b
        out dx, al

        mov al, 11110111b
        out dx, al

;; 2，初始化入口地址
        mov ax, 0000h
        mov ds, ax

        mov word ptr [0ach], offset int0
        mov word ptr [0aeh], seg int0
        sti
        mov cl, 00h

;; 3，中断服务程序
int0:
        push ax
        push bx
        push cx
        sti
        inc cl
        cmp cl, 20
        jnz  exit
        mov cl, 00h
        mov al, 01000000b
        mov dx, 0343h
        out dx, al
        mov dx, 0341h
        in  al, dx
        mov bh, al
        in  al, dx
        mov bl, al
        neg bx
        inc bx

        lea ax, buf
        add [ax], bx
        adc [bx + 2], 0

        mov al, 00h
        mov dx, 0341h
        out dx, al
        out dx, al
exit:
        mov al, 00000011b
        mov dx, 0320h
        out dx, al
        pop cx
        pop bx
        pop ax
        iret

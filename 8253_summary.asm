.model .small
.8086
data segment
data ends

code segment
        assume cs:code, ds:data
start:
        ;; 初始化8253
        mov al, 00110000b
        out 43h, al
        mov al, 00h
        out 40h, al
        out 40h, al
cal:
        mov al, 00000000b
        out 43h, al

        in  al, 40h
        mov bl, al
        in  al, 40h
        mov bh, al
        ;; 因为8253是减数计数
        neg bx
        ;; 因为第一个人没有加进去
        inc bx
        ret
code ends
        end start
* 8253使用注意事项
** 1，读数之前要锁存。
** 2，模式3,也就是十六位的时候要读写两次。
** 3，模式3,十六位模式时先读写低八位，再读写高八位。
** 4，{写完锁存指令之后要等一个机器周期，也就是一个nop指令。（这个还不确定）。}
** 5，因为工作方式0,加入初始值过一个机器周期后才进行计数。
        ;; 8255综合应用
        ;; initilize 8086.
        mov al, 10010000b
        out 86h, al
        mov al, 0ffh
        out 84h, al

lop:    in  al, 80h
        rhr al, 1
        jnc first
        rar al, 1
        jnc second
        jmp lop
first:
        mov al, 00000000b
        out 86h, al
        mov al, 00000011b
        out 86h, al
        jmp lop
second:
        mov al, 00000001b
        out 86h, al
        mov al, 00000010b
        out 86h, al
        jmp lop
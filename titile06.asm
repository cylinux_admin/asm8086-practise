.model .small
.8086    
;define the data segment
data segment
dat1 db 01h, 08h, 03h, 05h, 05h    
     db 07h, 06h, 02h, 09h, 0ah   
num  db $-dat1                   
data ends               

;define the code segment
code segment            
    assume cs:code, ds:data
main proc far
    mov ax, data
    mov ds, ax     
    mov ch, 00h
    mov cl, num
    lea si, dat1 
lop:
    jcxz exit
    call sort
    loop lop
exit:
    hlt    
endp  

;The defination of the sort function.
sort proc near
    push cx
    push si
sort_loop:
    mov al, [si]  
    cmp al, [si + 1]  
    jle next
    xchg al, [si + 1]    
    mov [si], al
next:                
    inc  si
    loop sort_loop
    pop  si
    pop  cx  
ret    
endp
code ends  
    end
.model .small
.8086
;define the data segment
data segment
buffer  db 11h, 22h, 33h, 44h, 55h
        db 66h, 77h, 88h, 99h, 00h
sum     dw 0000h
avr     dw 00h
data ends

;define the code segment
code segment
    assume cs:code, ds:data
main proc far
    mov ax, data
    mov ds, ax
    mov si, offset buffer
    mov cx, 10
    clc
    mov ax, 0000h
start:
    add al, [si]
    daa
    jnc continue
    inc ah
continue:
    inc si
    loop start
    mov sum, ax
    sar ax,4
    mov avr, ax
    hlt
endp
code ends
    end
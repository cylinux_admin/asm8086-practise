;; 在8088非缓冲系统中扩展一片8259A，采用全嵌套方式工作，中断请求为边沿触发方式。
;; 中断类型码 50H~57H，只开放IR0和IR7。8259A端口地址80H，81H。

        ;; 只写初始化程序
;; 8259a初始化
        ;; ICW1
        mov al, 13h
        out 80h, al

        ;; ICW2
        mov al, 50h
        out 81h, al

        ;; ICW4
        mov al, 01h
        out 81h, al

        ;; OCW1
        mov al, 7eh
        out 81h, al
;; 8086中断向量表初始化
        mov ax, 0
        mov ds, ax

        mov ax, offset int0
        mov [140h], ax
        mov ax, seg int0
        mov [142h], ax

        mov ax, offset int1
        mov [15ch], ax
        mov ax, seg int1
        mov [15eh], ax
.model .small
.8086    
;define the data segment
data segment
dat1 db 00h, 01h, 02h, 03h, 04h
     db 05h, 06h, 07h, 08h, 09h
     db 10h, 11h, 2eh, 13h, 2eh
     db 14h, 2eh, 90h, 72h, 2eh  
dat2 db 10 dup(0)
data ends               

;define the code segment
code segment            
    assume cs:code, ds:data
main proc far
    mov ax, data
    mov ds, ax
    lea si, dat1
    lea di, dat2
    mov cx, 20
    mov dl, 0
start:
    mov al, [si]
    cmp al, 2eh
    jnz handle
    inc dl 
    mov al, 00h
handle:        
    mov [di], al  
    inc si
    inc di
    loop start
endp        
code ends    
    end 
.model .small
.8086    
;define the data segment
data segment
var1 dw 1234h    
var2 db 4 dup(0)
data ends               

;define the code segment
code segment            
    assume cs:code, ds:data
main proc far  
    mov ax, data
    mov ds, ax 
    lea si, var2
    mov cx, 04h
    mov dx, var1
handle:         
    mov ax, dx
    and al, 0fh
    or  al, 30h
    cmp al, 3ah
    jb  blow
    add al, 07h
blow:    
    mov [si], al
    ror dx, 4
    loop    handle
endp        
code ends   
end